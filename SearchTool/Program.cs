﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SearchTool
{
    static class Program
    {
        private static List<string> files;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SearchForm());
        }
    }
}
