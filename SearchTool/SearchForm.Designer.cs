﻿using System.Threading;

namespace SearchTool
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.foundFilesTreeView = new System.Windows.Forms.TreeView();
            this.resultInfoBox = new System.Windows.Forms.GroupBox();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.textLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.textTextBox = new System.Windows.Forms.TextBox();
            this.pathLabel = new System.Windows.Forms.Label();
            this.searchButton = new System.Windows.Forms.Button();
            this.searchComponentsBox = new System.Windows.Forms.GroupBox();
            this.filesCounterLabel = new System.Windows.Forms.Label();
            this.filesCounter = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.logInfoBox = new System.Windows.Forms.GroupBox();
            this.currentFileName = new System.Windows.Forms.Label();
            this.processingFileLabel = new System.Windows.Forms.Label();
            this.pauseButton = new System.Windows.Forms.Button();
            this.newSearchButton = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.infoMessage = new System.Windows.Forms.Label();
            this.resultInfoBox.SuspendLayout();
            this.searchComponentsBox.SuspendLayout();
            this.logInfoBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // foundFilesTreeView
            // 
            this.foundFilesTreeView.BackColor = System.Drawing.SystemColors.Control;
            this.foundFilesTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.foundFilesTreeView.Location = new System.Drawing.Point(6, 15);
            this.foundFilesTreeView.Name = "foundFilesTreeView";
            this.foundFilesTreeView.ShowNodeToolTips = true;
            this.foundFilesTreeView.Size = new System.Drawing.Size(286, 405);
            this.foundFilesTreeView.TabIndex = 0;
            // 
            // resultInfoBox
            // 
            this.resultInfoBox.Controls.Add(this.foundFilesTreeView);
            this.resultInfoBox.Location = new System.Drawing.Point(501, 12);
            this.resultInfoBox.Name = "resultInfoBox";
            this.resultInfoBox.Size = new System.Drawing.Size(298, 426);
            this.resultInfoBox.TabIndex = 9;
            this.resultInfoBox.TabStop = false;
            this.resultInfoBox.Text = "Результаты поиска";
            // 
            // pathTextBox
            // 
            this.pathTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SearchTool.Properties.Settings.Default, "SearchPathText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.pathTextBox.Location = new System.Drawing.Point(189, 15);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(280, 22);
            this.pathTextBox.TabIndex = 1;
            this.pathTextBox.Text = global::SearchTool.Properties.Settings.Default.SearchPathText;
            // 
            // textLabel
            // 
            this.textLabel.AutoSize = true;
            this.textLabel.Location = new System.Drawing.Point(6, 74);
            this.textLabel.Name = "textLabel";
            this.textLabel.Size = new System.Drawing.Size(128, 17);
            this.textLabel.TabIndex = 4;
            this.textLabel.Text = "Текст для поиска:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SearchTool.Properties.Settings.Default, "SearchNameText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nameTextBox.Location = new System.Drawing.Point(189, 43);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(280, 22);
            this.nameTextBox.TabIndex = 3;
            this.nameTextBox.Text = global::SearchTool.Properties.Settings.Default.SearchNameText;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(6, 46);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(149, 17);
            this.nameLabel.TabIndex = 2;
            this.nameLabel.Text = "Имя файла (RegExp):";
            // 
            // textTextBox
            // 
            this.textTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SearchTool.Properties.Settings.Default, "SearchText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textTextBox.Location = new System.Drawing.Point(189, 71);
            this.textTextBox.Name = "textTextBox";
            this.textTextBox.Size = new System.Drawing.Size(280, 22);
            this.textTextBox.TabIndex = 5;
            this.textTextBox.Text = global::SearchTool.Properties.Settings.Default.SearchText;
            // 
            // pathLabel
            // 
            this.pathLabel.AutoSize = true;
            this.pathLabel.Location = new System.Drawing.Point(6, 18);
            this.pathLabel.Name = "pathLabel";
            this.pathLabel.Size = new System.Drawing.Size(173, 17);
            this.pathLabel.TabIndex = 0;
            this.pathLabel.Text = "Адрес папки для поиска:";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(393, 101);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 33);
            this.searchButton.TabIndex = 6;
            this.searchButton.Text = global::SearchTool.Properties.Resources.searchButtonText;
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // searchComponentsBox
            // 
            this.searchComponentsBox.Controls.Add(this.searchButton);
            this.searchComponentsBox.Controls.Add(this.pathLabel);
            this.searchComponentsBox.Controls.Add(this.textTextBox);
            this.searchComponentsBox.Controls.Add(this.nameLabel);
            this.searchComponentsBox.Controls.Add(this.nameTextBox);
            this.searchComponentsBox.Controls.Add(this.textLabel);
            this.searchComponentsBox.Controls.Add(this.pathTextBox);
            this.searchComponentsBox.Location = new System.Drawing.Point(12, 12);
            this.searchComponentsBox.Name = "searchComponentsBox";
            this.searchComponentsBox.Size = new System.Drawing.Size(483, 140);
            this.searchComponentsBox.TabIndex = 6;
            this.searchComponentsBox.TabStop = false;
            // 
            // filesCounterLabel
            // 
            this.filesCounterLabel.AutoSize = true;
            this.filesCounterLabel.Location = new System.Drawing.Point(6, 51);
            this.filesCounterLabel.Name = "filesCounterLabel";
            this.filesCounterLabel.Size = new System.Drawing.Size(148, 17);
            this.filesCounterLabel.TabIndex = 11;
            this.filesCounterLabel.Text = "Обработано файлов:";
            // 
            // filesCounter
            // 
            this.filesCounter.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.filesCounter.Location = new System.Drawing.Point(160, 51);
            this.filesCounter.Name = "filesCounter";
            this.filesCounter.Size = new System.Drawing.Size(94, 17);
            this.filesCounter.TabIndex = 12;
            this.filesCounter.Text = "0";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(6, 81);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(54, 17);
            this.timeLabel.TabIndex = 13;
            this.timeLabel.Text = "Время:";
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.Location = new System.Drawing.Point(66, 81);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(64, 17);
            this.time.TabIndex = 14;
            this.time.Text = "00:00:00";
            // 
            // logInfoBox
            // 
            this.logInfoBox.Controls.Add(this.currentFileName);
            this.logInfoBox.Controls.Add(this.processingFileLabel);
            this.logInfoBox.Controls.Add(this.time);
            this.logInfoBox.Controls.Add(this.filesCounterLabel);
            this.logInfoBox.Controls.Add(this.timeLabel);
            this.logInfoBox.Controls.Add(this.filesCounter);
            this.logInfoBox.Location = new System.Drawing.Point(12, 334);
            this.logInfoBox.Name = "logInfoBox";
            this.logInfoBox.Size = new System.Drawing.Size(483, 104);
            this.logInfoBox.TabIndex = 15;
            this.logInfoBox.TabStop = false;
            // 
            // currentFileName
            // 
            this.currentFileName.AutoEllipsis = true;
            this.currentFileName.Location = new System.Drawing.Point(181, 18);
            this.currentFileName.Name = "currentFileName";
            this.currentFileName.Size = new System.Drawing.Size(296, 17);
            this.currentFileName.TabIndex = 16;
            // 
            // processingFileLabel
            // 
            this.processingFileLabel.AutoSize = true;
            this.processingFileLabel.Location = new System.Drawing.Point(6, 18);
            this.processingFileLabel.Name = "processingFileLabel";
            this.processingFileLabel.Size = new System.Drawing.Size(169, 17);
            this.processingFileLabel.TabIndex = 15;
            this.processingFileLabel.Text = "Обрабатываемый файл:";
            // 
            // pauseButton
            // 
            this.pauseButton.Enabled = false;
            this.pauseButton.Location = new System.Drawing.Point(236, 300);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(125, 29);
            this.pauseButton.TabIndex = 16;
            this.pauseButton.Tag = "pause";
            this.pauseButton.Text = global::SearchTool.Properties.Resources.pauseButtonText;
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // newSearchButton
            // 
            this.newSearchButton.Enabled = false;
            this.newSearchButton.Location = new System.Drawing.Point(367, 300);
            this.newSearchButton.Name = "newSearchButton";
            this.newSearchButton.Size = new System.Drawing.Size(114, 29);
            this.newSearchButton.TabIndex = 17;
            this.newSearchButton.Text = global::SearchTool.Properties.Resources.newSearchButtonText;
            this.newSearchButton.UseVisualStyleBackColor = true;
            this.newSearchButton.Click += new System.EventHandler(this.newSearchButton_Click);
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // infoMessage
            // 
            this.infoMessage.Location = new System.Drawing.Point(12, 159);
            this.infoMessage.Name = "infoMessage";
            this.infoMessage.Size = new System.Drawing.Size(483, 129);
            this.infoMessage.TabIndex = 18;
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 450);
            this.Controls.Add(this.infoMessage);
            this.Controls.Add(this.newSearchButton);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.logInfoBox);
            this.Controls.Add(this.resultInfoBox);
            this.Controls.Add(this.searchComponentsBox);
            this.Name = "SearchForm";
            this.Text = "Поиск";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SearchForm_FormClosing);
            this.resultInfoBox.ResumeLayout(false);
            this.searchComponentsBox.ResumeLayout(false);
            this.searchComponentsBox.PerformLayout();
            this.logInfoBox.ResumeLayout(false);
            this.logInfoBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TreeView foundFilesTreeView;
        private System.Windows.Forms.GroupBox resultInfoBox;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.Label textLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox textTextBox;
        private System.Windows.Forms.Label pathLabel;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.GroupBox searchComponentsBox;
        private System.Windows.Forms.Label filesCounterLabel;
        private System.Windows.Forms.Label filesCounter;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.GroupBox logInfoBox;
        private System.Windows.Forms.Label currentFileName;
        private System.Windows.Forms.Label processingFileLabel;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button newSearchButton;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label infoMessage;
    }
}

