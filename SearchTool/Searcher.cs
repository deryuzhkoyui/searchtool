﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace SearchTool
{
    public class Searcher
    {
        public delegate void StringDelegate(string s);
        public delegate void StringListDelegate(IEnumerable<string> s);
        public delegate void IntDelegate(int i);
        public delegate void VoidDelegate();

        public event StringDelegate _startProcessingFile;
        public event IntDelegate _finishProcessingFile;
        public event StringListDelegate _fileFound;
        public event VoidDelegate _paused;
        public event VoidDelegate _finished;

        private string _path;
        private string _name;
        private string _text;
        private int _filesCounter;

        private int _state; // 0 - processing, 1 - paused

        /// <summary>
        /// Поиск по указанным критериям
        /// </summary>
        /// <param name="path">Адрес папки для поиска</param>
        /// <param name="name">Имя файла для поиска</param>
        /// <param name="text">Текст для поиска внутри файла</param>
        public void Search(string path, string name, string text)
        {
            if (string.IsNullOrEmpty(path))
                throw new FileNotFoundException("Введите адрес папки для поиска!");
            if (!Directory.Exists(path) && !File.Exists(path))
                throw new FileNotFoundException("По указанному адресу не найдена папка!");

            //Инициализация полей
            _state = 0;
            _filesCounter = 0;

            _path = path;
            _name = BuildNamePattern(name);
            _text = text;

            SearchInFiles();
        }

        /// <summary>
        /// Поставить поиск на паузу
        /// </summary>
        public void Pause()
        {
            _state = 1;
        }

        /// <summary>
        /// Возобновить поиск
        /// </summary>
        public void Resume()
        {
            _state = 0;
            SearchInFiles();
        }

        private void SearchInFiles()
        {
            //Отдельная обработка на случай, если путь указан к файлу, а не к папке
            if (!File.GetAttributes(_path).HasFlag(FileAttributes.Directory))
            {
                ProcessFile(_path);
                return;
            }

            var entries = Directory.EnumerateFileSystemEntries(_path, "*", SearchOption.AllDirectories);
            var index = 0;
            foreach (var entry in entries)
            {
                index++;
                if (_state != 0)
                    break;

                //Счетчик для возобновления поиска с места остановки
                if (index <= _filesCounter)
                    continue;
                ProcessFile(entry);
            }

            if (_state == 1) //paused
                _paused();
            else
                _finished();
        }

        /// <summary>
        /// Проверка содержания указанного текста в файле
        /// </summary>
        /// <param name="path">Адрес файла</param>
        private bool ContainsText(string path)
        {
            if (string.IsNullOrEmpty(_text))
                return true;

            if (File.GetAttributes(path).HasFlag(FileAttributes.Directory))
                return false;

            using (var reader = new StreamReader(path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains(_text))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Проверка файла на соответствие критериев поиска:
        /// имени и содержимого
        /// </summary>
        /// <param name="path">Адрес файла</param>
        private void ProcessFile(string path)
        {
            //OnStartProcessingFileAction?.Invoke(path);
            _startProcessingFile(path);

            if (Regex.IsMatch(Path.GetFileName(path), _name)
                && ContainsText(path))
                _fileFound(path
                    .Replace(_path, "")
                    .Split(Path.DirectorySeparatorChar)
                    .Where((item) => !string.IsNullOrEmpty(item)));
            
            _finishProcessingFile(++_filesCounter);
        }

        /// <summary>
        /// Метод преобразования текста имени с вайлдкартами
        /// в регулярное выражение
        /// </summary>
        private string BuildNamePattern(string name)
        {
            if (string.IsNullOrEmpty(name)) return ".*";
            var sb = new StringBuilder("^");
            foreach (var character in name)
            {
                if (Regex.IsMatch(character.ToString(), "\\.|\\^|\\$|\\[|\\]|\\(|\\)|\\+|\\=|\\!|\\{|\\}"))
                {
                    sb.Append("\\" + character);
                    continue;
                }

                if (character == '?')
                {
                    sb.Append(".");
                    continue;
                }

                if (character == '*')
                { 
                    sb.Append(".*");
                    continue;
                }
                sb.Append(character);
            }
            sb.Append("$");
            return sb.ToString();
        }
    }
}
