﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SearchTool
{
    public partial class SearchForm : Form
    {
        private Searcher _searchTool;
        private TreeNodeCollection _fileNodes;
        private int _timeElapsed;

        public SearchForm()
        {
            InitializeComponent();

            _fileNodes = foundFilesTreeView.Nodes;
            _searchTool = new Searcher();

            #region Добавляем слушателей событий для объекта Searcher
            _searchTool._startProcessingFile += ((fileName) =>
            {
                var changeCurrentFileName = new Action(() =>
                {
                    currentFileName.Text = fileName;
                    currentFileName.Refresh();
                });
                this.Invoke(changeCurrentFileName);
            });

            _searchTool._finishProcessingFile += ((filesCount) =>
            {
                var changeCurrentFileCounter = new Action(() =>
                {
                    filesCounter.Text = filesCount.ToString();
                    filesCounter.Refresh();
                });
                this.Invoke(changeCurrentFileCounter);
            });

            _searchTool._fileFound += ((filePath) =>
            {
                var foundFile = new Action(() =>
                {
                    var currNode = _fileNodes[0];
                    //Для каждого элемента пути к файлу добавляем новый узел                    
                    foreach (var pathElement in filePath)
                    {
                        int index = currNode.Nodes.IndexOfKey(pathElement);
                        if (index < 0)
                            //Если этого элемента пути не существует, добавялем его
                            currNode = currNode.Nodes.Add(pathElement, pathElement);
                        else
                            //Если существует - проваливаемся в него
                            currNode = currNode.Nodes[index];
                        currNode.Parent.Expand();
                    }
                    foundFilesTreeView.Refresh();
                });
                this.Invoke(foundFile);
            });

            _searchTool._paused += (() =>
            {
                var paused = new Action(() =>
                {
                    newSearchButton.Enabled = true;
                    timer.Stop();
                });
                this.Invoke(paused);
            });

            _searchTool._finished += (() =>
            {
                var finished = new Action(() =>
                {
                    EnableSearchInputs();
                    pauseButton.Text = Properties.Resources.pauseButtonText;
                    pauseButton.Tag = "pause";
                    pauseButton.Enabled = false;
                    newSearchButton.Enabled = false;
                    timer.Stop();
                });
                this.Invoke(finished);
            });
            #endregion
        }

        private void ResetDefaults()
        {
            _timeElapsed = 0;
            
            foundFilesTreeView.Nodes.Clear();
            foundFilesTreeView.Refresh();

            filesCounter.Text = "0";
            filesCounter.Refresh();

            currentFileName.Text = "";
            currentFileName.Refresh();

            time.Text = "00:00:00";
            time.Refresh();
        }

        private void DisableSearchInputs()
        {
            searchButton.Enabled = false;
            pathTextBox.Enabled = false;
            nameTextBox.Enabled = false;
            textTextBox.Enabled = false;
        }

        private void EnableSearchInputs()
        {
            searchButton.Enabled = true;
            pathTextBox.Enabled = true;
            nameTextBox.Enabled = true;
            textTextBox.Enabled = true;
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            SearchAsync();
        }

        private async void SearchAsync()
        {
            ResetDefaults();
            DisableSearchInputs();
            pauseButton.Enabled = true;
            timer.Start();

            string path = pathTextBox.Text;
            string name = nameTextBox.Text;
            string text = textTextBox.Text;

            _fileNodes.Add(path);
            foundFilesTreeView.Refresh();

            try
            {
                await Task.Run(() => _searchTool.Search(path, name, text));
            }
            catch (Exception ex)
            {
                infoMessage.Text = ex.Message;
            }
        }

        private void SearchForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.SearchPathText = pathTextBox.Text;
            Properties.Settings.Default.SearchNameText = nameTextBox.Text;
            Properties.Settings.Default.SearchText = textTextBox.Text;
            Properties.Settings.Default.Save();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            var changeTimeText = new Action(() =>
            {
                time.Text = TimeSpan.FromSeconds(++_timeElapsed).ToString(@"hh\:mm\:ss");
                time.Refresh();
            });

            this.Invoke(changeTimeText);
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            switch (pauseButton.Tag)
            {
                case ("pause"):
                    {
                        _searchTool.Pause();
                        pauseButton.Text = Properties.Resources.playButtonText;
                        pauseButton.Tag = "play";
                        pauseButton.Refresh();
                        break;
                    }
                case ("play"):
                    {
                        pauseButton.Text = Properties.Resources.pauseButtonText;
                        pauseButton.Tag = "pause";
                        pauseButton.Refresh();

                        newSearchButton.Enabled = false;
                        Task.Run(() =>_searchTool.Resume());
                        timer.Start();
                        break;
                    }
            }
        }

        private void newSearchButton_Click(object sender, EventArgs e)
        {
            ResetDefaults();
            EnableSearchInputs();
            pauseButton.Text = Properties.Resources.pauseButtonText;
            pauseButton.Tag = "pause";
            pauseButton.Enabled = false;
            newSearchButton.Enabled = false;
        }
    }
}
